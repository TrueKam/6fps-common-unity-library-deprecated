# 6fps Common Unity Library

The 6fps Common Unity Library aims to be a useful library of scripts for Unity-based projects. Originally meant for internal use at 6fps Network, the decision was made to publish the library for public use.