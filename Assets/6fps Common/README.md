# 6fps Common Unity Library

This library is a collection of commonly-used scripts and functionality that I've found myself rewriting every time I try to start a new project in Unity.