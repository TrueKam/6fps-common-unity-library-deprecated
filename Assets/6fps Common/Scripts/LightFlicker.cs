﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightFlicker : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("What is the shortest amount of time that the light should be off?")]
    private float ShortestOffTime = 0.05f;

    [SerializeField]
    [Tooltip("What is the longest amount of time that the light should be off?")]
    private float LongestOffTime = 0.50f;

    [SerializeField]
    [Tooltip("What is the shortest amount of time that the light should be on?")]
    private float ShortestOnTime = 0.10f;

    [SerializeField]
    [Tooltip("What is the longest amount of time that the light should be on?")]
    private float LongestOnTime = 1.00f;

    private bool CurrentlyOn = true;
    private float SwitchTimer = 0.00f;

    private float OriginalIntensity;
    private Light LightRef;

    // This can be made editable if there is a desire for being able to set the on and off
    // intensity, but right now it's only on (original intensity) and off (zero intensity).
    private const float OffIntensity = 0.00f;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        LightRef = GetComponent<Light>( );

        OriginalIntensity = LightRef.intensity;
        SwitchTimer = Random.Range( ShortestOnTime, LongestOnTime );
    }

    
    void Update()
    {
        if ( Time.time > SwitchTimer )
        {
            SwitchTimer = Time.time + ( CurrentlyOn ? Random.Range( ShortestOffTime, LongestOffTime ) : Random.Range( ShortestOnTime, LongestOnTime ) );
            LightRef.intensity = ( CurrentlyOn ? 0.0f : OriginalIntensity );
            CurrentlyOn = !CurrentlyOn;
        }
    }
    #endregion
}
