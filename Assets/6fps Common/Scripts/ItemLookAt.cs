﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLookAt : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("Look at the player? Overridden to false if a GameObject is set in LookAtObject.")]
    private bool LookAtPlayer = true;

    [SerializeField]
    [Tooltip("If a GameObject is set here, LookAtPlayer will be overridden to false.")]
    private GameObject LookAtObject = default;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        // Looking at two different objects every frame seems like a really bad idea. "Flickering" tends
        // to be the result. If a GameObject is set in LookAtObject, then don't look at the player.
        if ( LookAtObject )
        {
            LookAtPlayer = false;
        }
        else
        {
            LookAtPlayer = true;
        }
    }

    void Update()
    {
        if ( LookAtPlayer )
        {
            transform.LookAt( GameObject.FindGameObjectWithTag( "Player" ).transform );
        }
        else if ( LookAtObject )
        {
            transform.LookAt( LookAtObject.transform );
        }
        else
        {
            Debug.LogError( "ItemLookAt: LookAtPlayer is false, but no GameObject was defined in LookAtObject. Destroying script." );
            Destroy( this );
        }
    }
    #endregion

    void SetLookAtPlayer ( bool lookAtPlayer )
    {
        LookAtPlayer = lookAtPlayer;
    }

    void SetObjectToLookAt( GameObject newGameObject )
    {
        LookAtObject = newGameObject;
        LookAtPlayer = false;
    }
}
