﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotate : MonoBehaviour
{

    #region Variables
    [SerializeField]
    private bool PauseRotationWhenGamePaused = true;

    [SerializeField]
    private bool RotateOnX = false;

    [SerializeField]
    private float RotateOnXSpeedMultiplier = 1.0f;

    [SerializeField]
    private bool RotateOnY = true;

    [SerializeField]
    private float RotateOnYSpeedMultiplier = 1.0f;

    [SerializeField]
    private bool RotateOnZ = false;

    [SerializeField]
    private float RotateOnZSpeedMultiplier = 1.0f;

    private float BasicRotationIncrement = 0.1f;
    #endregion

    #region Unity Built-In Functionality
    void Update()
    {
        float XRotation = BasicRotationIncrement * RotateOnXSpeedMultiplier * ( PauseRotationWhenGamePaused ? Time.deltaTime : 1.0f );
        float YRotation = BasicRotationIncrement * RotateOnYSpeedMultiplier * ( PauseRotationWhenGamePaused ? Time.deltaTime : 1.0f );
        float ZRotation = BasicRotationIncrement * RotateOnZSpeedMultiplier * ( PauseRotationWhenGamePaused ? Time.deltaTime : 1.0f );

        transform.Rotate( new Vector3( ( RotateOnX ? XRotation : 0f ), ( RotateOnY ? YRotation : 0f ), ( RotateOnZ ? ZRotation : 0f ) ) );
    }
    #endregion
}
