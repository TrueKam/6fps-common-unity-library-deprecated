using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// RandomRotation allows a GameObject to be randomly rotated to any increment of 45 or 90 degrees.
/// 
/// Future enhancement should focus on making the increment a double input and finding a more generic
/// method of calculating the random rotation increment instead of hard-coding the random number
/// range based on the increment enum chosen.
/// </summary>
public class RandomRotation : MonoBehaviour
{
    enum Axis { X, Y, Z };
    enum Increment { FortyFive, Ninety };

    #region Variables
    [SerializeField][Tooltip("What axis should the GameObject rotate around?")]
    private Axis RotationAxis = Axis.Y;

    [SerializeField]
    [Tooltip( "What increment should the GameObject be rotated to?" )]
    private Increment RotationIncrement = Increment.Ninety;
    #endregion

    #region Unity Built-In Functionality
    /// <summary>
    /// On Start, rotate the GameObject based on the increment.
    /// </summary>
    void Start ()
    {
        float rotationAmount = 0.0f;
        switch ( RotationIncrement )
        {
            case Increment.FortyFive:
                rotationAmount = Random.Range( 0, 7 ) * 45.0f;
                break;
            case Increment.Ninety:
                rotationAmount = Random.Range( 0, 3 ) * 90.0f;
                break;
        }

        Vector3 rotationVector = new Vector3();
        switch ( RotationAxis )
        {
            case Axis.X:
                rotationVector = Vector3.right;
                break;
            case Axis.Y:
                rotationVector = Vector3.up;
                break;
            case Axis.Z:
                rotationVector = Vector3.forward;
                break;
        }

        transform.Rotate( rotationVector, rotationAmount );
        Destroy( this );
    }
    #endregion
}
