﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBob : MonoBehaviour
{

    #region Variables
    [SerializeField]
    [Tooltip("Pause this GameObject's movement when game is paused via setting time scaling to 0?")]
    private bool PauseWhenGamePaused = true;

    [SerializeField]
    [Tooltip("How much should the GameObject move? (This number should represent the difference in Y value from top to bottom.)")]
    private float MovementAmount = 0.5f;

    [SerializeField]
    [Tooltip("Multiplier for GameObject's bobbing speed.")]
    private float MovementSpeedMultiplier = 1.0f;

    [SerializeField][Tooltip("Should the movement start at a random point in its cycle?")]
    private bool StartAtRandomPoint = false;

    // Can be tweaked for testing, but can be ignored in favor of Serialized MovementSpeedMultiplier.
    private const float BasicCounterIncrement = 20.0f;

    private float MovementCounter = 0.0f;
    private float OriginalYValue;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        OriginalYValue = transform.position.y;

        if ( StartAtRandomPoint )
        {
            MovementCounter = Random.Range( 0.0f, 360.0f );
        }
    }

    void Update()
    {

        MovementCounter += BasicCounterIncrement * MovementSpeedMultiplier * ( PauseWhenGamePaused ? Time.deltaTime : 1.0f );
        if ( MovementCounter >= 360f )
        {
            MovementCounter -= 360f;
        }

        float NewPosition = Mathf.Sin( Mathf.Deg2Rad * MovementCounter ) * 0.5f * MovementAmount;

        transform.position = new Vector3( transform.position.x, OriginalYValue + NewPosition, transform.position.z );
        
    }
    #endregion
}
