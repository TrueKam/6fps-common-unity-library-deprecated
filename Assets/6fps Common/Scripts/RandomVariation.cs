﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SixFPS.Utils;

public class RandomVariation : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("The original prototype to be removed when the game starts.")]
    private GameObject Prototype = null;

    [SerializeField]
    [Tooltip("All the possible variations to be used.")]
    private GameObject[] PossibleVariations = null;

    [SerializeField]
    [Tooltip("Add integer weights to indices matching the indices in PossibleVariations if weighted randomization is desired. Must have as many integers as possible variations.")]
    private int[] VariationWeights = null;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        if ( !Prototype || PossibleVariations.Length == 0 )
        {
            Debug.LogError( "RandomVariations on " + name + " is missing a prototype or possible variations. Destroying script." );
            Destroy( this );
        }

        Prototype.SetActive( false );
        GameObject.Instantiate( PossibleVariations[GetRandomIndex( )], transform.position, transform.rotation, null );
        Destroy( gameObject );
    }
    #endregion

    /// <summary>
    /// Gets a random index for which random variation to use.
    /// </summary>
    /// <returns>The index of the random variation to use.</returns>
    private int GetRandomIndex ()
    {
        return ( UseWeights( ) ? WeightedRandom.WeightedRandomInt( VariationWeights ) : Random.Range( 0, PossibleVariations.Length ) );
    }

    /// <summary>
    /// Determines whether weights should be used in calculating which random variation to use.
    /// </summary>
    /// <returns>A Boolean value indicating whether or not to use weights.</returns>
    private bool UseWeights ()
    {
        bool UsingWeights = false;

        if ( PossibleVariations.Length == VariationWeights.Length )
        {
            UsingWeights = true;
        }

        return UsingWeights;
    }
}
