﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightPulse : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("Should the light pulsing effect pause if the game is paused? (Paused is determined by checking Time.timeScale = 0.0f.)")]
    private bool PauseWhenGamePaused = true;

    [SerializeField]
    [Tooltip("What is the highest intensity the light should get to?")]
    private float HighestIntensity = 40_000.00f;

    [SerializeField]
    [Tooltip("What is the lowest intensity the light should get to?")]
    private float LowestIntensity = 0.00f;

    [SerializeField]
    [Tooltip("If you don't like the speed that the light pulses at, you can change it here. Higher numbers mean faster pulsing.")]
    private float SpeedMultiplier = 100.00f;

    private const float BaseCounterIncrement = 1.0f;

    private float Counter;
    private Light LightRef;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        LightRef = GetComponent<Light>( );

        // Choose a random starting point. Unless the player starts within sight of the light and reloads multiple times,
        // they'll probably never notice.
        Counter = Random.Range( 0f, 360f );

        // This code doesn't currently have any checks to ensure that the highest and lowest intensities are different. Users
        // won't do that, anyway, right? Right?
        if ( LowestIntensity > HighestIntensity )
        {
            Debug.LogWarning( "LightPulse on " + name + " has a LowestIntensity greater than HighestIntensity. Swapping." );
            float temp = LowestIntensity;
            LowestIntensity = HighestIntensity;
            HighestIntensity = temp;
        }
    }

    void Update()
    {
        if ( PauseWhenGamePaused && Mathf.Approximately( 0f, Time.timeScale ) )
            return;

        // This is a long calculation, but it's not complex enough to warrant breaking into a separate function.
        LightRef.intensity = ( Mathf.Sin( Counter * Mathf.Deg2Rad ) * ( HighestIntensity - LowestIntensity )* 0.5f )
                             + ( ( HighestIntensity + LowestIntensity ) * 0.5f );
        Counter += BaseCounterIncrement * SpeedMultiplier * Time.deltaTime;
    }
    #endregion
}
