﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightMorseCodeFlash : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private string InputString = "Enter text to encode.";

    [SerializeField]
    private float SpeedMultiplier = 1.0f;

    private string EncodedString;
    private bool CurrentlyOn;
    private Light LightRef;
    private int PositionCounter;
    private float OriginalIntensity;
    private float SwitchTimer;

    private const float DashTime  = 0.15f;
    private const float DotTime   = 0.05f;
    private const float OffTime   = 0.05f;
    private const float PauseTime = 0.05f; // 3 units between letters. Sandwiched between two OffTimes, accounts for the proper timing.
    private const float SpaceTime = 0.25f; // 7 units between words. Sandwichd between two OffTimes, accounts for the proper timing.
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        if ( InputString.Length == 0 )
        {
            Debug.LogWarning( "LightMorseCodeFlash on " + name + " has an empty input string. Removing script." );
            Destroy( this );
        }

        EncodedString = Encode( InputString.ToUpper() );
        PositionCounter = 0;
        LightRef = GetComponent<Light>( );
        OriginalIntensity = LightRef.intensity;
    }
    
    void Update()
    {
        if ( Time.time > SwitchTimer )
        {
            if ( CurrentlyOn )
            {
                // Turn Off
                LightRef.intensity = 0f;
                SwitchTimer = Time.time + OffTime / SpeedMultiplier;
            }
            else
            {
                // Turn On
                char CurrentCharacter = EncodedString[PositionCounter];

                switch ( CurrentCharacter )
                {
                    case '-':
                        LightRef.intensity = OriginalIntensity;
                        SwitchTimer = Time.time + DashTime / SpeedMultiplier;
                        break;
                    case '.':
                        LightRef.intensity = OriginalIntensity;
                        SwitchTimer = Time.time + DotTime / SpeedMultiplier;
                        break;
                    case '<': // Special case for pause between letters.
                        LightRef.intensity = 0f;
                        SwitchTimer = Time.time + PauseTime / SpeedMultiplier;
                        break;
                    case '>': // Special case for pause between words.
                        LightRef.intensity = 0f;
                        SwitchTimer = Time.time + SpaceTime / SpeedMultiplier;
                        break;
                    default:
                        Debug.LogWarning( "LightMorseCodeFlash on " + name + " has encountered an unexpected character in the encoded string." );
                        break;
                }

                CurrentlyOn = false;

                PositionCounter++;
                if ( PositionCounter == EncodedString.Length )
                {
                    PositionCounter = 0;
                }
            }
        }
    }
    #endregion

    /// <summary>
    /// Encodes the provided string as a series of dots and dashes per the International Morse Code.
    /// Pauses between characters are represented with a '<' symbol and pauses between words are
    /// represented by a '>' symbol. Characters not supported by the International Morse Code are
    /// not handled, but are skipped.
    /// </summary>
    /// <param name="StringToEncode">The input string to encode in Morse Code.</param>
    /// <returns>The string encoded in Morse Code.</returns>
    private string Encode ( string StringToEncode )
    {
        string FinishedString = "";

        // Handle special cases:
        //   Pause between letters: <
        //   Pause between words:   >

        foreach ( char Character in StringToEncode )
        {


            switch ( char.ToUpper(Character) )
            {
                case 'A':
                    FinishedString += ".-<";
                    break;
                case 'B':
                    FinishedString += "-...<";
                    break;
                case 'C':
                    FinishedString += "-.-.<";
                    break;
                case 'D':
                    FinishedString += "-..<";
                    break;
                case 'E':
                    FinishedString += ".<";
                    break;
                case 'F':
                    FinishedString += "..-.<";
                    break;
                case 'G':
                    FinishedString += "--.<";
                    break;
                case 'H':
                    FinishedString += "....<";
                    break;
                case 'I':
                    FinishedString += "..<";
                    break;
                case 'J':
                    FinishedString += ".---<";
                    break;
                case 'K':
                    FinishedString += "-.-.<";
                    break;
                case 'L':
                    FinishedString += ".-..<";
                    break;
                case 'M':
                    FinishedString += "--<";
                    break;
                case 'N':
                    FinishedString += "-.<";
                    break;
                case 'O':
                    FinishedString += "---<";
                    break;
                case 'P':
                    FinishedString += ".--.<";
                    break;
                case 'Q':
                    FinishedString += "--.-<";
                    break;
                case 'R':
                    FinishedString += ".-.<";
                    break;
                case 'S':
                    FinishedString += "...<";
                    break;
                case 'T':
                    FinishedString += "-<";
                    break;
                case 'U':
                    FinishedString += "..-<";
                    break;
                case 'V':
                    FinishedString += "...-<";
                    break;
                case 'W':
                    FinishedString += ".--<";
                    break;
                case 'X':
                    FinishedString += "-..-<";
                    break;
                case 'Y':
                    FinishedString += "-.--<";
                    break;
                case 'Z':
                    FinishedString += "--..<";
                    break;
                case '1':
                    FinishedString += ".----<";
                    break;
                case '2':
                    FinishedString += "..---<";
                    break;
                case '3':
                    FinishedString += "...--<";
                    break;
                case '4':
                    FinishedString += "....-<";
                    break;
                case '5':
                    FinishedString += ".....<";
                    break;
                case '6':
                    FinishedString += "-....<";
                    break;
                case '7':
                    FinishedString += "--...<";
                    break;
                case '8':
                    FinishedString += "---..<";
                    break;
                case '9':
                    FinishedString += "----.<";
                    break;
                case '0':
                    FinishedString += "-----<";
                    break;
                case ' ':
                    FinishedString += "><";
                    break;
                default:
                    break;
            }
        }

        return FinishedString;
    }
}
