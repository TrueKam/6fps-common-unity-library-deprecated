﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SixFPS.Utils
{

    public static class WeightedRandom
    {
        /// <summary>
        /// Gets a randomly determined index based on the incoming array of weights.
        /// </summary>
        /// <param name="weights">Array of integers containing weights to use in calculations.</param>
        /// <returns></returns>
        public static int WeightedRandomInt( int[] weights )
        {

            if ( weights.Length == 0 )
            {
                Debug.LogWarning( "SixFPS.Utils.WeightedRandom: Incoming variable 'weights' contains no values." );
                return 0;
            }

            int CumulativeSum = 0;
            int RandomValue;
            int ReturnValue = -1;

            foreach ( int currVal in weights )
            {
                CumulativeSum += currVal;
            }

            RandomValue = Random.Range( 0, CumulativeSum );

            for ( int currIdx = 0; currIdx < weights.Length; ++currIdx )
            {
                if ( RandomValue < weights[currIdx] )
                {
                    ReturnValue = currIdx;
                    break;
                }
                else
                {
                    RandomValue -= weights[currIdx];
                }
            }

            if ( ReturnValue == -1 )
            {
                ReturnValue = weights.Length - 1;
            }

            return ReturnValue;

        }
    }

}
