﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightProximityIntensity : MonoBehaviour
{
    #region Variables
    [SerializeField]
    [Tooltip("This is the close radius within which the NearbyIntensity will be used. It should be greater than 0 and less than the FarAwayRadius.")]
    private float NearbyRadius = 5.0f;

    [SerializeField]
    [Tooltip("This is the far radiums beyond which the FarAwayIntensity will be used. It should be greater than the NearbyRadius.")]
    private float FarAwayRadius = 15.0f;

    [SerializeField]
    [Tooltip("This intensity will be used when the tracked GameObject is at or within the NearbyRadius. Smooth scaling between this and the FarAwayIntensity will be used between the two radii.")]
    private float NearbyIntensity = 0.0f;

    [SerializeField]
    [Tooltip("This intensity will be used when the tracked GameObject is at or beyond the FarAwayRadius. Smooth scaling between this and the FarAwayIntensity will be used between the two radii.")]
    private float FarAwayIntensity = 10.0f;

    [SerializeField]
    [Tooltip("If this is true, track the player. The player is determined by finding the GameObject in the current scene with the \"Player\" tag. Multiple GameObjects on the \"Player\" layer will potentially cause unexpected results.")]
    private bool TrackPlayer = true;

    [SerializeField]
    [Tooltip("If this is not null, this GameObject will be tracked instead of the player. If this is set, TrackPlayer is ignored.")]
    private GameObject ObjectToTrack = null;

    private Light LightRef;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        if ( NearbyRadius < 0.0f )
        {
            NearbyRadius = 0.0f;
        }
        if ( NearbyRadius > FarAwayRadius )
        {
            float tempRadius = NearbyRadius;
            NearbyRadius = FarAwayRadius;
            FarAwayRadius = tempRadius;
        }
        if ( ObjectToTrack != null )
        {
            TrackPlayer = false;
        }
        if ( TrackPlayer )
        {
            ObjectToTrack = GameObject.FindGameObjectWithTag( "Player" );
        }
        if ( ObjectToTrack == null )
        {
            Debug.LogError( "LightProximityIntensity on " + name + " has no GameObject to track. Did you forget to set it? Is there an object with the tag \"Player\"?" );
            Destroy( this );
        }

        LightRef = GetComponent<Light>( );

        // If the script has gotten this far, the near and far radii are set and there is a reference in the ObjectToTrack variable.
    }

    void Update()
    {
        float TrackingDistance = Vector3.Distance( transform.position, ObjectToTrack.transform.position );

        if ( TrackingDistance > FarAwayRadius )
        {
            LightRef.intensity = FarAwayIntensity;
        }
        else if ( TrackingDistance < FarAwayRadius && TrackingDistance > NearbyRadius )
        {
            float DistanceBetweenRadii = ( TrackingDistance - NearbyRadius) / ( FarAwayRadius - NearbyRadius);
            LightRef.intensity = Mathf.Lerp( NearbyIntensity, FarAwayIntensity, DistanceBetweenRadii );
        }
        else if ( TrackingDistance < NearbyRadius )
        {
            LightRef.intensity = NearbyIntensity;
        }
        else
        {
            Debug.LogWarning( "LightProximityIntensity on " + name + " has encountered a state that the developer didn't expect. Congratulations." );
        }
    }
    #endregion
}
