﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightFlash : MonoBehaviour
{

    #region Variables
    [SerializeField]
    [Tooltip("What is the intensity to use when the light is \"on?\" (Set to 0 to use light intensity in editor.)")]
    private float HighIntensity = 40_000.00f;

    [SerializeField]
    [Tooltip("What is the intensity to use when the light is \"off?\"")]
    private float LowIntensity = 0.00f;

    [SerializeField]
    [Tooltip("How long should the light be \"on?\"")]
    private float TimeAtHighIntensity = 0.50f;

    [SerializeField]
    [Tooltip("How long should the light be \"off?\"")]
    private float TimeAtLowIntensity = 0.50f;

    [SerializeField]
    [Tooltip("Should the light switching timer pause when the game is paused?")]
    private bool PauseWhenGamePaused = true;

    private float SwitchTimer = 0.00f;
    private bool CurrentlyOn = false;

    private Light LightRef;
    #endregion

    #region Unity Built-In Functionality
    void Start()
    {
        LightRef = GetComponent<Light>( );
        if ( HighIntensity == 0 )
        {
            HighIntensity = LightRef.intensity;
        }

        if ( Random.Range( 0, 1 ) == 0 ) // Start Off
        {
            LightRef.intensity = LowIntensity;
            SwitchTimer = Time.time + TimeAtLowIntensity;
            CurrentlyOn = false;
        }
        else // Start On
        {
            LightRef.intensity = HighIntensity;
            SwitchTimer = Time.time + TimeAtHighIntensity;
            CurrentlyOn = true;
        }
    }

    void Update()
    {
        if ( PauseWhenGamePaused && Mathf.Approximately( 0.0f, Time.timeScale ) )
            return;

        if ( Time.time >= SwitchTimer )
        {
            LightRef.intensity = ( CurrentlyOn ? LowIntensity : HighIntensity );
            SwitchTimer = Time.time + ( CurrentlyOn ? TimeAtLowIntensity : TimeAtHighIntensity );
            CurrentlyOn = !CurrentlyOn;
        }
    }
    #endregion
}
