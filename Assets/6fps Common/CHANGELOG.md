# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2021-02-16
### Added
- RandomRotation
### Updated
- Light Scripts: Changed default light intensity to values more appropriate for HDRP.

## [1.0.0] - 2020-08-18
### Added
- Example Scene: RandomVariations
- Basic Rain Asset
- ItemBob
- ItemBounce
- ItemLookAt
- ItemRotate
- LightFlash
- LightFlicker
- LightMorseCodeFlash
- LightProximityIntensity
- LightPulse
- RandomVariation
- WeightedRandom
- Test Scene: WeightedRandom Test Scene
- Test Script: WeightedRandomTester