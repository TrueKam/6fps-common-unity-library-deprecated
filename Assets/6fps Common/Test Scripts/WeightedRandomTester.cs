﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SixFPS.Utils;

public class WeightedRandomTester : MonoBehaviour
{
    int[] weights = { 1, 2, 3, 4, 5 };
    int[] results = { 0, 0, 0, 0, 0 };

    [SerializeField]
    [Tooltip("How many iterations to run? More will increase runtime, but will produce more \"expected\" results.")]
    uint iterations = 1_000_000_000;

    void Start()
    {
        Debug.Log( "WeightedRandomTester: Weights          = {    1,    2,    3,    4,    5 }" );
        Debug.Log( "WeightedRandomTester: Expected Results = { 0.07, 0.13, 0.20, 0.27, 0.33 }" );

        for ( uint counter = 0; counter < iterations; ++counter )
        {
            results[WeightedRandom.WeightedRandomInt( weights )]++;
        }

        string resultsOutput;

        resultsOutput =  "{ ";
        resultsOutput += ( (float) results[0] / (float) iterations ).ToString( "0.00" );
        resultsOutput += ", ";
        resultsOutput += ( (float) results[1] / (float) iterations ).ToString( "0.00" );
        resultsOutput += ", ";
        resultsOutput += ( (float) results[2] / (float) iterations ).ToString( "0.00" );
        resultsOutput += ", ";
        resultsOutput += ( (float) results[3] / (float) iterations ).ToString( "0.00" );
        resultsOutput += ", ";
        resultsOutput += ( (float) results[4] / (float) iterations ).ToString( "0.00" );
        resultsOutput += " }";

        Debug.Log( "WeightedRandomTester: Actual Results   = " + resultsOutput );
    }
}
